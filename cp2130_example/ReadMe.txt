
AN792SW - CP2130 LibUSB Sample v1.2 Release Notes
Copyright (C) 2014-2020 Silicon Laboratories, Inc.

This release contains the following source files:

        * cp2130ex.cc
        * Makefile
        * SiliconLabs.rules (for Linux only - copy to /etc/udev/rules.d/)
        * ReadMe.txt (this file)

Refer to AN792 for an explanation of the source code.


Known issues and limitations:
-----------------------------

	1.) Only CP2130 devices are supported by this sample source. 


Dependencies:
-------------

    libusb-1.0


Instructions for installing libusb:
-----------------------------------

    Homebrew:

       $ brew install libusb
      
    MacPorts:
      
       $ sudo port install libusb

    Linus (e.g. Ubuntu)
    
       $ sudo apt-get install libusb-1.0-0-dev


How to build libusb_example:
----------------------------

  $ make

  Note: If your header file is not found, use the following instead:

  $ HEADERS_SEARCH_FLAGS=-I<location of libusb-1.0 folder> bash -c "make"


Revision History:
-----------------

    version 1.0     July 16, 2014

	  - Initial Release

    version 1.1     July 12, 2019


    version 1.2     May 15, 2020

      - Read TransferPriority to determine Write/Read endpoint assignments.

        Note: The code snippets in Application Note AN792 Rev 0.3 sections 
        8.1.5.2 (Bulk OUT Requests) and 8.1.5.3 (Bulk IN Requests) both use 
        endpoint 0x01, which is incorrect. This example code should be used, 
        instead of the code examples printed in AN792.

      - Fix command-buffer size error in Read example.

      - Add WriteRead example.
